#include <stm32f10x.h>
#include <stdio.h>

int main()
{
	// Configuration
	// enable register page 146
	RCC->APB2ENR |= (1<<2);
	// set pin 13 to be output with
	GPIOA->CRH = 0x00300000;
	while(1)
	{
		//loop
		GPIOA->ODR = 0x0000;	// set pin = 0
		for(int i=0; i<100000;i++);		// delay
	}
	return 0;
}