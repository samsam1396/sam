#include <stm32f10x.h>
#include <stdio.h>

int main()
{
	// Configuration
	// enable register page 146
	RCC->APB2ENR |= (1<<2);
	// All pin to be output with
	GPIOA->CRL = 0x00000333;
	//GPIOC->CRH = 0x00300000;
	int POS=1;
	while(1)
	{
		
		switch(POS)
		{
			case 1:
				GPIOA->ODR |= (1<<0);	// set led 1 on
			  //for(int i=0; i<10000000;i++);		// delay
				GPIOA->ODR &= ~(1<<1);	// set led 2 off
				GPIOA->ODR &= ~(1<<2);// set led 3 off
				break;
			case 2:
				GPIOA->ODR &= ~(1<<0);	// set led 1 off
				GPIOA->ODR |= (1<<1);	// set led 2 on
			  //for(int i=0; i<10000000;i++);		// delay
				GPIOA->ODR &= ~(1<<2);	// set led 3 off
				break;
			case 3:
				GPIOA->ODR &= ~(1<<0);	// set led 1 off
				GPIOA->ODR &= ~(1<<1);	// set led 2 on
				GPIOA->ODR |= (1<<2);	// set led 3 off
			  //for(int i=0; i<10000000;i++);		// delay
				break;
		}
		for(int i=0; i<1000000;i++);		// delay
		POS++;
		if(POS>3){
			POS=1;
		}
	}
	return 0;
}