#include <stm32f10x.h>
#include <stdio.h>
long int i;
int main(){
	
	//enable clock for GPIOC from APB2ENR RCC(reset clock and control module)
	RCC->APB2ENR |= (1<<4)|(1<<2);
	//configuration [one time]
	//PinC13 to be output with high speed [3]  0x40011004
	GPIOA->CRL = 0x00000004;
	GPIOC->CRH = 0x00300000;  
	
	while(1){
		//loop
		if(GPIOA->IDR & 0x0001){ 
		GPIOC->ODR &=~ (1<<13);
		for(i=0;i<10000;i++);    // delay
		}
		else{
			GPIOC->ODR |= (1<<13);
		for(i=0;i<10000;i++);    // delay
	}
}
	return 0;
}