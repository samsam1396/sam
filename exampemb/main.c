#include <stm32f10x.h>
#include <stdio.h>
long int i;
int main(){
	
	//enable clock for GPIOC from APB2ENR RCC(reset clock and control module)
	RCC->APB2ENR |= (1<<2);
	//configuration [one time]
	//PinC13 to be output with high speed [3]  0x40011004
	GPIOA->CRL = 0x33333333;
	GPIOA->CRH = 0x33333333;  
	
	while(1){
		//loop
		GPIOA->ODR = 0x0000;   //reset pins = 0
		for(i=0;i<10000;i++);    // delay
		GPIOA->ODR = 0xffff ;  //set pins = 1
		for(i=0;i<10000;i++);     //delay
	}
	return 0;
}